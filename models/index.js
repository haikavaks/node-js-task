/**
 * Created by Avakian on 12/1/2018.
 */
import Product from './Product';
import User from './User';


export {
    Product,
    User
}
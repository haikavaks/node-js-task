const csv = require('csvtojson');
const fs = require('fs');

import { EventEmitter }  from './../dirwatcher/DirWatcher';

export default class Importer {
    constructor(){
        EventEmitter.on('dirwatcher:changed', this.import)
    }

    import(files, path) {
        files.map((file) => {
           csv().fromFile(path + '/' + file).then(jsonObj=> console.log(jsonObj));
        });
    }

    importSync(files, path) {
        files.map((file) => {
            let csvOutput = [];
            const fileContents = fs.readFileSync(path + '/' + file);
            const lines = fileContents.toString().split('\n');

            for (let i = 0; i < lines.length; i++) {
                csvOutputObject.push(lines[i].toString().split(','));
            }
            console.log(csvOutput)
        });
    }
}
const fs = require('fs');
const Events = require('events');

const EventEmitter = new Events.EventEmitter();

class DirWatcher {
    constructor() {
        this.files = {};
    }

    watch(path, delay) {
        setInterval(() => {
            fs.readdir(path, (err, scannedFiles) => {
                let modifiedFilesWithDates = {};
                const fn = function asyncGetModifiedDate(fileName) {
                    return new Promise(resolve => resolve({
                        fileName: fileName,
                        modifiedDate: fs.statSync(path + '/' + fileName).mtime.getTime()
                    }));
                };
                const promises = scannedFiles.map(fn);
                Promise.all(promises).then(modifiedFiles => {
                    modifiedFiles.forEach((value) => {
                        modifiedFilesWithDates[value.fileName] = value.modifiedDate
                    });

                    let diff = this.getDiff(this.files, modifiedFilesWithDates);
                    this.emitChanges(diff, path);
                    this.files = modifiedFilesWithDates;
                })
            })
        }, delay)
    }

    getDiff(previous, current) {
        let diff = [];
        if (Object.keys(previous).length) {
            for (let fileName in current) {
                if (previous[fileName] && previous[fileName] === current[fileName]) {
                    console.log('doesnt modified')
                } else {
                    console.log('file  change')
                    diff.push(fileName);
                }
            }
        } else if (Object.keys(current).length) {
            console.log('initial change')
            diff = Object.keys(current);
        }
        return diff;
    }

    emitChanges(diff, path) {
        EventEmitter.emit('dirwatcher:changed', diff, path);

    }

}
export {EventEmitter, DirWatcher}